import { loadRamoAtividades } from './../../store/ramo-atividade.actions';
import { AppState } from './../../store/root.reducer';
import { ClientDataForm } from './../../models/client-data.form.model';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { FormGroupState } from 'ngrx-forms';
import { Store } from '@ngrx/store';
import { RamoAtividadeState } from 'src/app/store/ramo-atividade.reducer';
import { updateShowBackButtonFlag, updateTitle } from 'src/app/store/toolbar.actions';

@Component({
  selector: 'app-client-data',
  templateUrl: './client-data.component.html',
  styleUrls: ['./client-data.component.sass'],
})
export class ClientDataComponent implements OnInit {
  clientDataForm$: Observable<FormGroupState<ClientDataForm>> = this.store.select(
    (state) => state.form.controls.clientData
  );

  formIsValid: Boolean;

  ramoAtividade$: Observable<RamoAtividadeState> = this.store.select(
    (state) => state.ramoAtividades
  );

  constructor(private store: Store<AppState>, private router: Router) {
    this.clientDataForm$
      .pipe(map((form) => form.isTouched && form.isValid))
      .subscribe((isValid) => (this.formIsValid = isValid));
  }

  ngOnInit(): void {
    this.store.dispatch(loadRamoAtividades());
    this.store.dispatch(updateTitle({title: 'Simulador'}));
    this.store.dispatch(updateShowBackButtonFlag({showBackButton: true}));
  }

  onSubmit(): void {
    
    if (!this.formIsValid) {
      return;
    }

    this.router.navigate(['fee-data']);
  }
}
