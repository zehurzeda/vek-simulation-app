import { getResultSimulation } from './../../store/result-data.actions';
import { loadConcorrentes } from './../../store/concorrente.actions';
import { ConcorrenteState } from './../../store/concorrente.reducer';
import { AppState } from './../../store/root.reducer';
import { FeeDataForm } from './../../models/fee-data.form.model';
import { FormGroupState } from 'ngrx-forms';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Store, select } from '@ngrx/store';
import { map } from 'rxjs/operators';
import { updateShowBackButtonFlag, updateTitle } from 'src/app/store/toolbar.actions';

@Component({
  selector: 'app-fee-data',
  templateUrl: './fee-data.component.html',
  styleUrls: ['./fee-data.component.sass'],
})
export class FeeDataComponent implements OnInit {
  feeDataForm$: Observable<FormGroupState<FeeDataForm>> = this.store.select(
    (state) => state.form.controls.feeData
  );

  formIsValid: Boolean;

  concorrente$: Observable<ConcorrenteState> = this.store.select(
    (state) => state.concorrente
  );

  constructor(private router: Router, private store: Store<AppState>) {
    this.feeDataForm$
      .pipe(map((form) => form.isTouched && form.isValid))
      .subscribe((isValid) => (this.formIsValid = isValid));
  }

  ngOnInit(): void {
    this.store.dispatch(loadConcorrentes());
    this.store.dispatch(updateTitle({title: 'Simulador'}));
    this.store.dispatch(updateShowBackButtonFlag({showBackButton: true}));
  }

  onSubmit(): void {
    this.store.dispatch(getResultSimulation());
  }
}
