import { clearResultData } from './../../store/result-data.actions';
import { AppState } from './../../store/root.reducer';
import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { ResultModel } from 'src/app/models/result.model';
import { Observable } from 'rxjs';
import { ResetAction, SetValueAction } from 'ngrx-forms';
import { initialFormState } from 'src/app/store/form.reducer';

@Component({
  selector: 'app-dialog-response',
  templateUrl: './dialog-response.component.html',
  styleUrls: ['./dialog-response.component.sass'],
})
export class DialogResponseComponent implements OnInit {
  public data$: Observable<ResultModel> = this.store.select(
    (state) => state.result
  );

  constructor(private router: Router, private store: Store<AppState>) {}

  ngOnInit(): void {}

  onClick() {
    this.store.dispatch(new SetValueAction(initialFormState.id, initialFormState.value));
    this.store.dispatch(new ResetAction(initialFormState.id));
    this.store.dispatch(clearResultData());
    this.router.navigate(['home']);
  }
}
