import { saveSimulation } from './../../store/result-data.actions';

import { ResultModel } from './../../models/result.model';
import { Router } from '@angular/router';
import { AppState } from './../../store/root.reducer';
import { Store, select } from '@ngrx/store';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import {
  updateShowBackButtonFlag,
  updateTitle,
} from 'src/app/store/toolbar.actions';

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.sass'],
})
export class ResultComponent implements OnInit {
  public result$: Observable<ResultModel> = this.store.pipe(
    select((state) => state.result)
  );

  constructor(private store: Store<AppState>, private router: Router) {}

  ngOnInit(): void {
    this.store.dispatch(updateTitle({ title: 'Simulador' }));
    this.store.dispatch(updateShowBackButtonFlag({ showBackButton: true }));
  }

  reajustar(): void {
    this.router.navigate(['fee-data']);
  }

  recusar(): void {
    this.store.dispatch(saveSimulation({ status: false }));
  }

  aceitar(): void {
    this.store.dispatch(saveSimulation({ status: true }));
  }
}
