import { updateShowBackButtonFlag, updateTitle } from './../../store/toolbar.actions';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/store';
import { getAllAcceptedReports } from 'src/app/store/result-data.actions';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  constructor(private store: Store<AppState>) { }

  ngOnInit(): void {
    this.store.dispatch(updateTitle({title: 'Simulação'}));
    this.store.dispatch(updateShowBackButtonFlag({showBackButton: false}));
  }

  generateReport(): void {
    this.store.dispatch(getAllAcceptedReports());
  }

}
