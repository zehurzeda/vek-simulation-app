import { PropostaReportModel } from './../models/proposta-repport.model';
import { PropostaSavedModel } from './../models/proposta-saved.model';
import { Injectable } from '@angular/core';
import { Angular2Csv } from 'angular2-csv';

@Injectable({
  providedIn: 'root',
})
export class ReportService {
  constructor() {}

  allAcceptedPropostaReport(propostas: PropostaSavedModel[]) {
    let data: PropostaReportModel[] = propostas.map(
      (proposta) => new PropostaReportModel(proposta)
    );

    console.log('data', data);
    let options = {
      title: 'Propostas aceitas',
      decimalseparator: '.',
      showLabels: true,
      showTitle: true,
      headers: [
        'Cpf/CNPJ',
        'Telefone',
        'E-mail',
        'Ramo Atividade',
        'Concorrente',
        'Taxa Concorrente Débito',
        'Taxa final aceita Débito',
        'Taxa Concorrente Crédito',
        'Taxa final aceita Crédito',
        'Data de aceite',
      ],
    };
    return new Angular2Csv(data, 'propostas_aceitas', options);
  }
}
