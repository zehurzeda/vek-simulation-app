import { environment } from 'src/environments/environment';
import { RamoAtividadeModel } from './../models/ramo-atividade.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RamoAtividadeService {

  constructor(private http: HttpClient) { }

  getRamoAtividades() {
    return this.http.get<RamoAtividadeModel[]>(`${environment.baseUrl}/ramo-atividade`);
  }

}
