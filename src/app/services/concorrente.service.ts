import { environment } from 'src/environments/environment';
import { ConcorrenteModel } from './../models/concorrente.model';
import { Injectable } from '@angular/core';
import { timer } from 'rxjs';
import { mapTo } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ConcorrenteService {

  constructor(private http: HttpClient) { }

  getConcorrentes() {
    return this.http.get<ConcorrenteModel[]>(`${environment.baseUrl}/concorrente`);
  }
}
