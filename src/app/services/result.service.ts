import { PropostaSavedModel } from './../models/proposta-saved.model';
import { environment } from 'src/environments/environment';
import { ResultModel } from './../models/result.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { PropostaModel } from '../models/proposta.model';

@Injectable({
  providedIn: 'root',
})
export class ResultService {
 
  private route: string;

  constructor(private http: HttpClient) {
    this.route = `${environment.baseUrl}proposta`;
  }

  simulateProposta(propostaData: PropostaModel) {
    return this.http.post<ResultModel>(`${this.route}/simulate`, propostaData);
  }

  saveProposta(propostaData: PropostaModel) {
    return this.http.post<PropostaSavedModel>(`${this.route}`, propostaData);
  }

  getAcceptedPropostas() {
    return this.http.get<PropostaSavedModel[]>(`${this.route}/accepted`);
  }

}
