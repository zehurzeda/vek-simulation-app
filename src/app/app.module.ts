import { ResultDataEffects } from './store/result-data.effects';
import { ConcorrenteEffects } from './store/concorrente.effects';
import { RamoAtividadeEffects } from './store/ramo-atividade.effects';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule, LOCALE_ID } from '@angular/core';

import { MaterialModule } from './material/material.module';
import { StoreModule } from '@ngrx/store';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgrxFormsModule } from 'ngrx-forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ToolbarComponent } from './toolbar/toolbar.component';
import { ClientDataComponent } from './components/client-data/client-data.component';
import { FeeDataComponent } from './components/fee-data/fee-data.component';
import { ResultComponent } from './components/result/result.component';
import { HomeComponent } from './components/home/home.component';
import { AcceptedProposalsComponent } from './components/accepted-proposals/accepted-proposals.component';
import { rootReducer } from './store';
import { NgxMaskModule } from 'ngx-mask';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { environment } from '../environments/environment';
import { registerLocaleData } from '@angular/common';
import pt from '@angular/common/locales/global/pt';
import {
  CurrencyMaskConfig,
  CurrencyMaskModule,
  CURRENCY_MASK_CONFIG,
} from 'ng2-currency-mask';
import { DialogResponseComponent } from './components/dialog-response/dialog-response.component';
import { HttpClientModule } from '@angular/common/http';

export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
  align: 'right',
  allowNegative: false,
  decimal: ',',
  precision: 2,
  prefix: '',
  suffix: '%',
  thousands: '.',
};

registerLocaleData(pt, 'pt-BR');

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    ClientDataComponent,
    FeeDataComponent,
    ResultComponent,
    HomeComponent,
    AcceptedProposalsComponent,
    DialogResponseComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgrxFormsModule,
    StoreModule.forRoot(rootReducer),
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MaterialModule,
    NgxMaskModule.forRoot(),
    EffectsModule.forRoot([
      RamoAtividadeEffects,
      ConcorrenteEffects,
      ResultDataEffects,
    ]),
    StoreDevtoolsModule.instrument({
      maxAge: 25,
      logOnly: environment.production,
    }),
    CurrencyMaskModule,
    HttpClientModule,
  ],
  providers: [
    { provide: LOCALE_ID, useValue: 'pt' },
    { provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
