import { ResultComponent } from './components/result/result.component';
import { FeeDataComponent } from './components/fee-data/fee-data.component';
import { ClientDataComponent } from './components/client-data/client-data.component';
import { HomeComponent } from './components/home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AcceptedProposalsComponent } from './components/accepted-proposals/accepted-proposals.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: '/home',
    pathMatch: 'full'
  },
  { path: 'home', component: HomeComponent },
  { path: 'client-data', component: ClientDataComponent },
  { path: 'fee-data', component: FeeDataComponent },
  { path: 'result', component: ResultComponent },
  { path: 'accepted-proposals', component: AcceptedProposalsComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
