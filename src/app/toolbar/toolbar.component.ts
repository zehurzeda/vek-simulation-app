import { ToolbarState } from './../store/toolbar.reducer';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { AppState } from '../store';
import {Location} from '@angular/common';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.sass'],
})
export class ToolbarComponent implements OnInit {
  public toolbar$: Observable<ToolbarState> = this.store.select(
    (state) => state.toolbar
  );

  constructor(private store: Store<AppState>, private _location: Location) {}

  ngOnInit(): void {}

  onClickBack() {
    this._location.back();
  }
}
