import { TaxaModel } from './../models/taxa.model';
import { FeeDataForm } from './../models/fee-data.form.model';

import { updateGroup, validate } from 'ngrx-forms';
import { required } from 'ngrx-forms/validation';

export const initialFeeDataState = {
  concorrenteId: null,
  debito: {
    desconto:null,
    taxaConcorrente: null
  },
  credito: {
    desconto:null,
    taxaConcorrente: null
  }
};

export const feeDataGroupValidation = updateGroup<FeeDataForm>({
  concorrenteId: validate(required),
  debito: updateGroup<TaxaModel>({
    desconto: validate(required),
    taxaConcorrente: validate(required)
  }),
  credito: updateGroup<TaxaModel>({
    desconto: validate(required),
    taxaConcorrente: validate(required)
  })
});