import { createReducer, on } from '@ngrx/store';
import * as ToolbarActions from './toolbar.actions';

export interface ToolbarState {
  title: string;
  showBackButton: Boolean;
}

export const initialToolbarState: ToolbarState = {
  title: '',
  showBackButton: false,
};

const _toolbarReducer = createReducer(
  initialToolbarState,
  on(ToolbarActions.updateTitle, (state, { title }) => ({
    ...state,
    title: title,
  })),
  on(ToolbarActions.updateShowBackButtonFlag, (state, { showBackButton }) => ({
    ...state,
    showBackButton: showBackButton,
  }))
);

export function toolbarReducer(state, action) {
  return _toolbarReducer(state, action);
}
