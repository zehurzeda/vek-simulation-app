import { ConcorrenteService } from './../services/concorrente.service';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as ConcorrenteActions from './concorrente.actions';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class ConcorrenteEffects {
  loadConcorrentes$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ConcorrenteActions.loadConcorrentes),
      mergeMap(() =>
        this.concorrenteService.getConcorrentes().pipe(
          map((concorrentes) =>
            ConcorrenteActions.loadSuccess({ concorrentes })
          ),
          catchError(() => of(ConcorrenteActions.loadError))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private concorrenteService: ConcorrenteService
  ) {}
}
