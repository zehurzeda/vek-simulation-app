import { ConcorrenteModel } from './../models/concorrente.model';
import { createReducer, on } from '@ngrx/store';
import * as ConcorrenteActions from './concorrente.actions';

export interface ConcorrenteState {
  data: Array<ConcorrenteModel>;
  isLoading: Boolean;
  isError: Boolean;
}

export const initialStateConcorrente: ConcorrenteState = {
  data: [],
  isLoading: false,
  isError: false
}

const _concorrenteReducer = createReducer(
  initialStateConcorrente,
  on(ConcorrenteActions.loadConcorrentes, () => ({
    isLoading: true,
    data: [],
    isError: false
  })),
  on(ConcorrenteActions.loadSuccess, (state, {concorrentes}) => ({
    data: concorrentes,
    isLoading: false,
    isError: false
  })),
  on(ConcorrenteActions.loadError, () => ({
    isError: true,
    data: [],
    isLoading: false
  }))
);

export function concorrenteReducer(state, action) {
  return _concorrenteReducer(state, action);
}