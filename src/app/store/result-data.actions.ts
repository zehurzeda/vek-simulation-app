import { ResultModel } from './../models/result.model';
import { createAction, props } from '@ngrx/store';

export const getResultSimulation = createAction('[Result Page] load result');

export const setResultData = createAction(
  '[Result Page] Set Result Data',
  props<{ result: ResultModel }>()
);

export const saveSimulation = createAction(
  '[Result Page] change result status',
  props<{ status: Boolean }>()
);

export const clearResultData = createAction('[Result Page] clear result');

export const getAllAcceptedReports = createAction(
  '[Home Page] get accepted report'
);
