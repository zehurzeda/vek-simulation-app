import { ResultModel } from './../models/result.model';
import { createReducer, on } from '@ngrx/store';
import * as ResultDataActions from './result-data.actions';

export const initialStateResult: ResultModel = {
  isValid: null,
  minValueDebit: null,
  minValueCredit: null,
  proposta: null
};

const _resultDataReducer = createReducer(
  initialStateResult,
  on(ResultDataActions.getResultSimulation, (state) => (state)),
  on(ResultDataActions.setResultData, (state, { result }) => result),
  on(ResultDataActions.saveSimulation, (state, {status}) => ({...state, proposta: {...state.proposta, isAccepted : status}})),
  on(ResultDataActions.clearResultData, () => (initialStateResult)),
  on(ResultDataActions.getAllAcceptedReports, (state) => (state))
);

export function resultDataReducer(state, action) {
  return _resultDataReducer(state, action);
}
