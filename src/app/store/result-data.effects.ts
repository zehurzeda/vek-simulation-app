import { ReportService } from './../services/report.service';
import { Router } from '@angular/router';
import { DialogResponseComponent } from './../components/dialog-response/dialog-response.component';
import { AppState } from './root.reducer';
import { Store } from '@ngrx/store';
import { ResultService } from './../services/result.service';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as ResultDataActions from './result-data.actions';
import {
  concatMap,
  exhaustMap,
  map,
  tap,
  withLatestFrom,
} from 'rxjs/operators';
import { of } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';

@Injectable()
export class ResultDataEffects {
  loadResultSimulation$ = createEffect(() =>
    this.actions$.pipe(
      ofType(ResultDataActions.getResultSimulation),
      concatMap((action) => of(action).pipe(withLatestFrom(this.store$.select((state) => state.form)))),
      exhaustMap(([action, form]) =>
        this.resultService
          .simulateProposta(
            {
              ...form.value.clientData,
              ...form.value.feeData,
              isAccepted: false
            }
          )
          .pipe(map((result) => ResultDataActions.setResultData({ result })))
      ),
      tap(() => {
        this.router.navigate(['result']);
      })
    )
  );

  updateResultSimulation$ = createEffect(
    () =>
      this.actions$.pipe(
        ofType(ResultDataActions.saveSimulation),
        concatMap((action) =>
          of(action).pipe(
            withLatestFrom(this.store$.select((state) => state.result))
          )
        ),
        exhaustMap(([action, result]) =>
          this.resultService.saveProposta({
            ...result.proposta,
            isAccepted: action.status,
          })
        ),
        tap(() => {
          this.dialog.open(DialogResponseComponent, { disableClose: true });
        })
      ),
    { dispatch: false }
  );

  getReportService$ = createEffect(
    () => 
      this.actions$.pipe(
        ofType(ResultDataActions.getAllAcceptedReports),
        exhaustMap(() => 
          this.resultService.getAcceptedPropostas().pipe(
            map((result) => this.reportService.allAcceptedPropostaReport(result))
          )
        )
      ),
    {dispatch: false}
  )

  constructor(
    private actions$: Actions,
    private resultService: ResultService,
    private store$: Store<AppState>,
    public dialog: MatDialog,
    private router: Router,
    private reportService: ReportService
  ) {}
}
