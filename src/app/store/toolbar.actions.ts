import { createAction, props } from '@ngrx/store';

export const updateTitle = createAction('[Toolbar] Update title', props<{title: string}>())
export const updateShowBackButtonFlag = createAction('[Toolbar] Update show back button flag', props<{showBackButton: Boolean}>())