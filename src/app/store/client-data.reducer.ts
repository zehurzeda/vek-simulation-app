import { RamoAtividadeModel } from './../models/ramo-atividade.model';
import { ClientDataForm } from './../models/client-data.form.model';

import { updateGroup, validate } from 'ngrx-forms';
import { required, email, pattern } from 'ngrx-forms/validation';

export const initialClientDataFormState = {
  cpfCnpj: null,
  telefone: null,
  email: null,
  ramoAtividadeId: null,
};

export const clientDataFormGroupValidation = updateGroup<ClientDataForm>({
  cpfCnpj: validate(required, pattern(new RegExp('\\b([0-9]{14}|[0-9]{11})\\b'))),
  telefone: validate(required, pattern(new RegExp('\\b([0-9]{11})\\b'))),
  email: validate(email),
  ramoAtividadeId: validate(required),
});
