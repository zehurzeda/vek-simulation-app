import { toolbarReducer, ToolbarState } from './toolbar.reducer';
import { concorrenteReducer, ConcorrenteState } from './concorrente.reducer';
import { ResultModel } from './../models/result.model';
import { formReducer } from './form.reducer';
import { resultDataReducer } from './result-data.reducer';
import { ramoAtividadeReducer, RamoAtividadeState } from './ramo-atividade.reducer';
import { FormGroupState } from 'ngrx-forms';
import { RootForm } from '../models/root.form.model';

export interface AppState {
  form: FormGroupState<RootForm>,
  result: ResultModel,
  ramoAtividades: RamoAtividadeState,
  concorrente: ConcorrenteState,
  toolbar: ToolbarState
}

export const rootReducer = {
  form: formReducer,
  result: resultDataReducer,
  ramoAtividades: ramoAtividadeReducer,
  concorrente: concorrenteReducer,
  toolbar: toolbarReducer
};
