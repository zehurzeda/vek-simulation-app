import { createReducer, on } from '@ngrx/store';
import { RamoAtividadeModel } from './../models/ramo-atividade.model';
import * as RamoAtividadeActions from './ramo-atividade.actions';

export interface RamoAtividadeState {
  data: Array<RamoAtividadeModel>;
  isLoading: Boolean;
  isError: Boolean;
}

export const initialStateRamoAtividade: RamoAtividadeState = {
  data: [],
  isLoading: false,
  isError: false,
};

const _ramoAtividadeReducer = createReducer(
  initialStateRamoAtividade,
  on(RamoAtividadeActions.loadRamoAtividades, () => ({
    isLoading: true,
    data: [],
    isError: false,
  })),
  on(RamoAtividadeActions.loadSuccess, (state, { ramoAtividades }) => ({
    data: ramoAtividades,
    isLoading: false,
    isError: false,
  })),
  on(RamoAtividadeActions.loadError, () => ({
    isLoading: false,
    data: [],
    isError: true,
  }))
);

export function ramoAtividadeReducer(state, action) {
  return _ramoAtividadeReducer(state, action);
}
