import { RamoAtividadeModel } from './../models/ramo-atividade.model';
import { createAction, props } from '@ngrx/store';

export const loadRamoAtividades = createAction(
  '[Ramo Atividade] load ramo atividade data'
);

export const loadSuccess = createAction(
  '[Ramo Atividade] load data success',
  props<{ ramoAtividades: Array<RamoAtividadeModel> }>()
);

export const loadError = createAction('[Ramo Atividade] load data error');
