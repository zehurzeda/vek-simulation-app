import { clientDataFormGroupValidation, initialClientDataFormState } from './client-data.reducer';
import { RootForm } from './../models/root.form.model';
import { feeDataGroupValidation, initialFeeDataState } from './fee-data.reducer';
import { updateGroup, onNgrxForms, wrapReducerWithFormStateUpdate, createFormGroupState } from 'ngrx-forms';
import { createReducer } from '@ngrx/store';

const validateFormState = updateGroup<RootForm>({
  clientData: clientDataFormGroupValidation,
  feeData: feeDataGroupValidation
})

export const initialFormState = createFormGroupState<RootForm>('form', {
  clientData: initialClientDataFormState,
  feeData: initialFeeDataState
});

const _formReducer = createReducer(initialFormState, onNgrxForms());

export const formReducer = wrapReducerWithFormStateUpdate(
  _formReducer,
  s => s,
  validateFormState
)