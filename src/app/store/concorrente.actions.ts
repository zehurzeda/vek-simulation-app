import { ConcorrenteModel } from './../models/concorrente.model';
import { createAction, props } from '@ngrx/store';

export const loadConcorrentes = createAction(
  '[Concorrente] load concorrente data'
);

export const loadSuccess = createAction(
  '[Concorrente] load concorrente success',
  props<{ concorrentes: Array<ConcorrenteModel> }>()
);

export const loadError = createAction('[Concorrente] load concorrente error')