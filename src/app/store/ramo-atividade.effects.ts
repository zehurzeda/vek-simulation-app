import { RamoAtividadeService } from './../services/ramo-atividade.service';
import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import * as RamoAtividadeActions from './ramo-atividade.actions';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { of } from 'rxjs';

@Injectable()
export class RamoAtividadeEffects {
  loadRamoAtividades$ = createEffect(() =>
    this.actions$.pipe(
      ofType(RamoAtividadeActions.loadRamoAtividades),
      mergeMap(() =>
        this.ramoAtividadeService.getRamoAtividades().pipe(
          map((ramoAtividades) =>
            RamoAtividadeActions.loadSuccess({ ramoAtividades })
          ),
          catchError(() => of(RamoAtividadeActions.loadError))
        )
      )
    )
  );

  constructor(
    private actions$: Actions,
    private ramoAtividadeService: RamoAtividadeService
  ) {}
}
