import { ConcorrenteModel } from './concorrente.model';
import { RamoAtividadeModel } from './ramo-atividade.model';
import { TaxaModel } from './taxa.model';

export interface PropostaSavedModel {
  cpfCnpj: string;
  telefone: string;
  email: string;
  ramoAtividade: RamoAtividadeModel;
  concorrente: ConcorrenteModel;
  debito: TaxaModel;
  credito: TaxaModel;
  isAccepted: Boolean;
  saveDate: string;
}