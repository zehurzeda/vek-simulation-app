import { TaxaModel } from './taxa.model';

export interface PropostaModel {
  cpfCnpj: string;
  telefone: string;
  email: string;
  ramoAtividadeId: number;
  concorrenteId: number;
  debito: TaxaModel;
  credito: TaxaModel;
  isAccepted: Boolean;
}