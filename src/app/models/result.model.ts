import { PropostaModel } from './proposta.model';


export interface ResultModel {
  isValid: Boolean;
  minValueDebit: number,
  minValueCredit: number,
  proposta: PropostaModel;
}