import { FeeDataForm } from './fee-data.form.model';
import { ClientDataForm } from './client-data.form.model';

export interface RootForm {
  clientData: ClientDataForm,
  feeData: FeeDataForm
}