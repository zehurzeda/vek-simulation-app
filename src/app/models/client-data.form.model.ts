import { RamoAtividadeModel } from './ramo-atividade.model';
import { Boxed } from 'ngrx-forms';

export interface ClientDataForm {
  cpfCnpj: string;
  telefone: string;
  email: string;
  ramoAtividadeId: number;
}