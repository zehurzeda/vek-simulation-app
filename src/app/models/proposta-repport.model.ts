import { PropostaSavedModel } from './proposta-saved.model';

export class PropostaReportModel {
  cpfCnpj: string;
  telefone: string;
  email: string;
  ramoAtividadeNome: string;
  concorrenteNome: string;
  taxaConcorrenteDebito: number;
  descontoDebito: number;
  taxaConcorrenteCredito: number;
  descontoCredito: number;
  saveDate: string;

  constructor(propostaSaved: PropostaSavedModel) {
    this.cpfCnpj = propostaSaved.cpfCnpj;
    this.telefone = propostaSaved.telefone;
    this.email = propostaSaved.email;
    this.ramoAtividadeNome = propostaSaved.ramoAtividade.nome;
    this.concorrenteNome = propostaSaved.concorrente.nome;
    this.taxaConcorrenteDebito = propostaSaved.debito.taxaConcorrente;
    this.descontoDebito = propostaSaved.debito.desconto;
    this.taxaConcorrenteCredito = propostaSaved.credito.taxaConcorrente;
    this.descontoCredito = propostaSaved.credito.desconto;
    this.saveDate = propostaSaved.saveDate;
  }
}