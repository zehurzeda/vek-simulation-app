import { TaxaModel } from './taxa.model';

export interface IResultData extends TaxaModel{
  type: string,
  minValue: number
}
