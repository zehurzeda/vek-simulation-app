import { TaxaModel } from './taxa.model';
import { ConcorrenteModel } from './concorrente.model';
import { Boxed } from 'ngrx-forms';

export interface FeeDataForm {
  concorrenteId: number;
  debito: TaxaModel;
  credito: TaxaModel;
}